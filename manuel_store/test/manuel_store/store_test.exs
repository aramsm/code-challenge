defmodule ManuelStore.StoreTest do
  use ManuelStore.DataCase

  alias ManuelStore.Store

  describe "products" do
    alias ManuelStore.Store.Product

    @valid_attrs %{
      attributes: %{},
      description: "some description",
      id: "7488a646-e31f-11e4-aace-600308960662",
      name: "some name"
    }
    @update_attrs %{
      attributes: %{},
      description: "some updated description",
      id: "7488a646-e31f-11e4-aace-600308960668",
      name: "some updated name"
    }
    @invalid_attrs %{attributes: nil, description: nil, id: nil, name: nil}

    def product_fixture(attrs \\ %{}) do
      {:ok, product} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Store.create_product()

      product
    end

    test "list_products/0 returns all products" do
      product = product_fixture()
      assert Store.list_products() == [product]
    end

    test "get_product!/1 returns the product with given id" do
      product = product_fixture()
      assert Store.get_product!(product.id) == product
    end

    test "create_product/1 with valid data creates a product" do
      assert {:ok, %Product{} = product} = Store.create_product(@valid_attrs)
      assert product.attributes == %{}
      assert product.description == "some description"
      assert product.id == "7488a646-e31f-11e4-aace-600308960662"
      assert product.name == "some name"
    end

    test "create_product/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Store.create_product(@invalid_attrs)
    end

    test "update_product/2 with valid data updates the product" do
      product = product_fixture()
      assert {:ok, %Product{} = product} = Store.update_product(product, @update_attrs)
      assert product.attributes == %{}
      assert product.description == "some updated description"
      assert product.id == "7488a646-e31f-11e4-aace-600308960668"
      assert product.name == "some updated name"
    end

    test "update_product/2 with invalid data returns error changeset" do
      product = product_fixture()
      assert {:error, %Ecto.Changeset{}} = Store.update_product(product, @invalid_attrs)
      assert product == Store.get_product!(product.id)
    end

    test "delete_product/1 deletes the product" do
      product = product_fixture()
      assert {:ok, %Product{}} = Store.delete_product(product)
      assert_raise Ecto.NoResultsError, fn -> Store.get_product!(product.id) end
    end
  end

  describe "stocks" do
    alias ManuelStore.Store.Stock

    @valid_attrs %{id: "7488a646-e31f-11e4-aace-600308960662", quantity: 42}
    @update_attrs %{id: "7488a646-e31f-11e4-aace-600308960668", quantity: 43}
    @invalid_attrs %{id: nil, quantity: nil}

    def stock_fixture(attrs \\ %{}) do
      {:ok, stock} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Store.create_stock()

      stock
    end

    test "list_stocks/0 returns all stocks" do
      stock = stock_fixture()
      assert Store.list_stocks() == [stock]
    end

    test "get_stock!/1 returns the stock with given id" do
      stock = stock_fixture()
      assert Store.get_stock!(stock.id) == stock
    end

    test "create_stock/1 with valid data creates a stock" do
      assert {:ok, %Stock{} = stock} = Store.create_stock(@valid_attrs)
      assert stock.id == "7488a646-e31f-11e4-aace-600308960662"
      assert stock.quantity == 42
    end

    test "create_stock/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Store.create_stock(@invalid_attrs)
    end

    test "update_stock/2 with valid data updates the stock" do
      stock = stock_fixture()
      assert {:ok, %Stock{} = stock} = Store.update_stock(stock, @update_attrs)
      assert stock.id == "7488a646-e31f-11e4-aace-600308960668"
      assert stock.quantity == 43
    end

    test "update_stock/2 with invalid data returns error changeset" do
      stock = stock_fixture()
      assert {:error, %Ecto.Changeset{}} = Store.update_stock(stock, @invalid_attrs)
      assert stock == Store.get_stock!(stock.id)
    end

    test "delete_stock/1 deletes the stock" do
      stock = stock_fixture()
      assert {:ok, %Stock{}} = Store.delete_stock(stock)
      assert_raise Ecto.NoResultsError, fn -> Store.get_stock!(stock.id) end
    end
  end

  describe "prices" do
    alias ManuelStore.Store.Price

    @valid_attrs %{
      currency: "some currency",
      id: "7488a646-e31f-11e4-aace-600308960662",
      value: 42
    }
    @update_attrs %{
      currency: "some updated currency",
      id: "7488a646-e31f-11e4-aace-600308960668",
      value: 43
    }
    @invalid_attrs %{currency: nil, id: nil, value: nil}

    def price_fixture(attrs \\ %{}) do
      {:ok, price} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Store.create_price()

      price
    end

    test "list_prices/0 returns all prices" do
      price = price_fixture()
      assert Store.list_prices() == [price]
    end

    test "get_price!/1 returns the price with given id" do
      price = price_fixture()
      assert Store.get_price!(price.id) == price
    end

    test "create_price/1 with valid data creates a price" do
      assert {:ok, %Price{} = price} = Store.create_price(@valid_attrs)
      assert price.currency == "some currency"
      assert price.id == "7488a646-e31f-11e4-aace-600308960662"
      assert price.value == 42
    end

    test "create_price/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Store.create_price(@invalid_attrs)
    end

    test "update_price/2 with valid data updates the price" do
      price = price_fixture()
      assert {:ok, %Price{} = price} = Store.update_price(price, @update_attrs)
      assert price.currency == "some updated currency"
      assert price.id == "7488a646-e31f-11e4-aace-600308960668"
      assert price.value == 43
    end

    test "update_price/2 with invalid data returns error changeset" do
      price = price_fixture()
      assert {:error, %Ecto.Changeset{}} = Store.update_price(price, @invalid_attrs)
      assert price == Store.get_price!(price.id)
    end
  end

  describe "orders" do
    alias ManuelStore.Store.Order

    @valid_attrs %{buy_date: ~N[2010-04-17 14:00:00], delivery_fee: 42, status: "some status"}
    @update_attrs %{
      buy_date: ~N[2011-05-18 15:01:01],
      delivery_fee: 43,
      status: "some updated status"
    }
    @invalid_attrs %{buy_date: nil, delivery_fee: nil, status: nil}

    def order_fixture(attrs \\ %{}) do
      {:ok, order} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Store.create_order()

      order
    end

    test "list_orders/0 returns all orders" do
      order = order_fixture()
      assert Store.list_orders() == [order]
    end

    test "get_order!/1 returns the order with given id" do
      order = order_fixture()
      assert Store.get_order!(order.id) == order
    end

    test "create_order/1 with valid data creates a order" do
      assert {:ok, %Order{} = order} = Store.create_order(@valid_attrs)
      assert order.buy_date == ~N[2010-04-17 14:00:00]
      assert order.delivery_fee == 42
      assert order.status == "some status"
    end

    test "create_order/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Store.create_order(@invalid_attrs)
    end

    test "update_order/2 with valid data updates the order" do
      order = order_fixture()
      assert {:ok, %Order{} = order} = Store.update_order(order, @update_attrs)
      assert order.buy_date == ~N[2011-05-18 15:01:01]
      assert order.delivery_fee == 43
      assert order.status == "some updated status"
    end

    test "update_order/2 with invalid data returns error changeset" do
      order = order_fixture()
      assert {:error, %Ecto.Changeset{}} = Store.update_order(order, @invalid_attrs)
      assert order == Store.get_order!(order.id)
    end

    test "delete_order/1 deletes the order" do
      order = order_fixture()
      assert {:ok, %Order{}} = Store.delete_order(order)
      assert_raise Ecto.NoResultsError, fn -> Store.get_order!(order.id) end
    end
  end

  describe "stocks_products" do
    alias ManuelStore.Store.StockProduct

    @valid_attrs %{quantity: 42}
    @update_attrs %{quantity: 43}
    @invalid_attrs %{quantity: nil}

    def stock_product_fixture(attrs \\ %{}) do
      {:ok, stock_product} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Store.create_stock_product()

      stock_product
    end

    test "list_stocks_products/0 returns all stocks_products" do
      stock_product = stock_product_fixture()
      assert Store.list_stocks_products() == [stock_product]
    end

    test "get_stock_product!/1 returns the stock_product with given id" do
      stock_product = stock_product_fixture()
      assert Store.get_stock_product!(stock_product.id) == stock_product
    end

    test "create_stock_product/1 with valid data creates a stock_product" do
      assert {:ok, %StockProduct{} = stock_product} = Store.create_stock_product(@valid_attrs)
      assert stock_product.quantity == 42
    end

    test "create_stock_product/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Store.create_stock_product(@invalid_attrs)
    end

    test "update_stock_product/2 with valid data updates the stock_product" do
      stock_product = stock_product_fixture()

      assert {:ok, %StockProduct{} = stock_product} =
               Store.update_stock_product(stock_product, @update_attrs)

      assert stock_product.quantity == 43
    end

    test "update_stock_product/2 with invalid data returns error changeset" do
      stock_product = stock_product_fixture()

      assert {:error, %Ecto.Changeset{}} =
               Store.update_stock_product(stock_product, @invalid_attrs)

      assert stock_product == Store.get_stock_product!(stock_product.id)
    end

    test "delete_stock_product/1 deletes the stock_product" do
      stock_product = stock_product_fixture()
      assert {:ok, %StockProduct{}} = Store.delete_stock_product(stock_product)
      assert_raise Ecto.NoResultsError, fn -> Store.get_stock_product!(stock_product.id) end
    end
  end

  describe "orders_products" do
    alias ManuelStore.Store.OrderProduct

    @valid_attrs %{quantity: 42}
    @update_attrs %{quantity: 43}
    @invalid_attrs %{quantity: nil}

    def order_product_fixture(attrs \\ %{}) do
      {:ok, order_product} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Store.create_order_product()

      order_product
    end

    test "list_orders_products/0 returns all orders_products" do
      order_product = order_product_fixture()
      assert Store.list_orders_products() == [order_product]
    end

    test "get_order_product!/1 returns the order_product with given id" do
      order_product = order_product_fixture()
      assert Store.get_order_product!(order_product.id) == order_product
    end

    test "create_order_product/1 with valid data creates a order_product" do
      assert {:ok, %OrderProduct{} = order_product} = Store.create_order_product(@valid_attrs)
      assert order_product.quantity == 42
    end

    test "create_order_product/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Store.create_order_product(@invalid_attrs)
    end

    test "update_order_product/2 with valid data updates the order_product" do
      order_product = order_product_fixture()

      assert {:ok, %OrderProduct{} = order_product} =
               Store.update_order_product(order_product, @update_attrs)

      assert order_product.quantity == 43
    end

    test "update_order_product/2 with invalid data returns error changeset" do
      order_product = order_product_fixture()

      assert {:error, %Ecto.Changeset{}} =
               Store.update_order_product(order_product, @invalid_attrs)

      assert order_product == Store.get_order_product!(order_product.id)
    end

    test "delete_order_product/1 deletes the order_product" do
      order_product = order_product_fixture()
      assert {:ok, %OrderProduct{}} = Store.delete_order_product(order_product)
      assert_raise Ecto.NoResultsError, fn -> Store.get_order_product!(order_product.id) end
    end
  end
end
