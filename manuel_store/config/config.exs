# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :manuel_store,
  ecto_repos: [ManuelStore.Repo]

# Configures the endpoint
config :manuel_store, ManuelStoreWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "vdkJmB8NTYS1RqWh+/wkbsRA1Nv0QYPyL+YNR0NL2MRN4rQQ9Wr60rkm8FeDRDp6",
  render_errors: [view: ManuelStoreWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: ManuelStore.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Configures Guardian https://github.com/ueberauth/guardian
config :manuel_store, ManuelStore.Accounts.Guardian,
  issuer: "manuel_store",
  secret_key: "tfR8EpAVLJZHtqqLGd7RPgyvLVjr8xqOykYC8nO1TgkAtOetXbGt9vodVFdGs56T"

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
