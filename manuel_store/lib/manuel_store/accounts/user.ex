defmodule ManuelStore.Accounts.User do
  use ManuelStore, :schema

  schema "users" do
    field(:email, :string)
    field(:name, :string)
    field(:password_hash, :string)
    field(:token, :string)
    field(:password, :string, virtual: true)

    has_many(:orders, Order)
    has_many(:orders_products, through: [:orders, :orders_products])

    timestamps()
  end

  @required_fields ~w(name email password token)
  def changeset(user, attrs) do
    user
    |> cast(attrs, @required_fields)
    |> validate_required(@required_fields)
    |> validate_format(:email, ~r/@/)
    |> hash_password
    |> unique_constraint(:email)
  end

  defp hash_password(%{valid?: false} = changeset), do: changeset

  defp hash_password(%{valid?: true, changes: %{password: password}} = changeset) do
    change(changeset, Argon2.add_hash(password))
  end
end
