defmodule ManuelStore.Accounts do
  @moduledoc """
  The Accounts context.
  """

  import Ecto.Query, warn: false
  alias ManuelStore.Repo

  alias ManuelStore.Accounts.User

  @doc """
  Authenticates a login request.

  Returns `{:ok, user}` if the login is valid,
  returns `{:error, "Wrong e-mail or password"}` otherwise.

  ## Examples

      iex> authenticate_user("example@email.com", "example_password")
      {:ok, %User{}}

      iex> authenticate_user("example@email.com", "invalid_password")
      {:error, "Wrong e-mail or password"}

  """
  def authenticate_user(email, password) do
    email
    |> get_user()
    |> verify_password(password)
  end

  defp verify_password(%User{} = user, password) do
    Argon2.verify_pass(password, user[:password_hash])
    |> case do
      true  -> {:ok, user}
      false -> verify_password(nil, password)
    end
  end

  defp verify_password(_, _) do
    Argon2.no_user_verify()
    {:error, "Wrong e-mail or password"}
  end

  @doc """
  Gets a single user.

  Returns `nil` if the User does not exist.

  ## Examples

      iex> get_user(123)
      %User{}

      iex> get_user(456)
      nil

  """
  def get_user(%{id: id}), do: Repo.get(User, id)

  @doc """
  Gets a user by e-mail.

  Returns `nil` if the User does not exist.

  ## Examples

      iex> get_user("teste@email.com")
      %User{}

      iex> get_user(456)
      nil

  """
  def get_user(email), do: Repo.get_by(User, email: email)

  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{field: value})
      {:ok, %User{}}

      iex> create_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a user.

  ## Examples

      iex> update_user(user, %{field: new_value})
      {:ok, %User{}}

      iex> update_user(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a User.

  ## Examples

      iex> delete_user(user)
      {:ok, %User{}}

      iex> delete_user(user)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user(%User{} = user), do: Repo.delete(user)
end
