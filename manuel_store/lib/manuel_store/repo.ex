defmodule ManuelStore.Repo do
  use Ecto.Repo,
    otp_app: :manuel_store,
    adapter: Ecto.Adapters.Postgres
end
