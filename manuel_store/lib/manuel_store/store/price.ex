defmodule ManuelStore.Store.Price do
  use ManuelStore, :schema

  schema "prices" do
    field(:currency, :string)
    field(:value, :integer)

    belongs_to(:product, Product)

    timestamps()
  end

  @required_fields ~w(currency value product_id)
  def changeset(price, attrs) do
    price
    |> cast(attrs, @required_fields)
    |> validate_required(@required_fields)
    |> check_constraint(:value, name: :value_must_be_positive)
  end
end
