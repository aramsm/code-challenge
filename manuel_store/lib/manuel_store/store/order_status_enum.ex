import EctoEnum

defenum ManuelStore.Store.OrderStatusEnum, :status, [:new, :approved, :cancelled, :delivered]
