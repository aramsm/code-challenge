defmodule ManuelStore.Store.StockProduct do
  use ManuelStore, :schema

  schema "stocks_products" do
    field :quantity, :integer

    belongs_to(:product, Product)
    belongs_to(:stock, Stock)

    has_many(:orders_products, OrderProduct)

    timestamps()
  end

  @required_fields ~w(quantity product_id stock_id)
  def changeset(stock_product, attrs) do
    stock_product
    |> cast(attrs, @required_fields)
    |> validate_required(@required_fields)
    |> check_constraint(:quantity, name: :quantity_must_be_positive)
  end
end
