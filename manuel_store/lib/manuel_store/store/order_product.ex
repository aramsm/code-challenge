defmodule ManuelStore.Store.OrderProduct do
  use ManuelStore, :schema

  schema "orders_products" do
    field :quantity, :integer

    belongs_to(:product, Product)
    belongs_to(:stock_product, StockProduct)
    belongs_to(:order, Order)
    has_one(:user, through: [:order, :user])
    has_one(:unit_price, through: [:product, :price])

    timestamps()
  end

  @required_fields ~w(quantity product_id order_id stock_product_id)
  def changeset(order_product, attrs) do
    order_product
    |> cast(attrs, @required_fields)
    |> validate_required(@required_fields)
    |> check_constraint(:quantity, name: :quantity_must_be_positive)
  end
end
