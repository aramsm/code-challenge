defmodule ManuelStore.Store.Stock do
  use ManuelStore, :schema

  schema "stocks" do
    field(:name, :string)

    many_to_many(
      :products,
      Product,
      join_through: "stocks_products",
      on_replace: :delete
    )

    timestamps()
  end

  @required_fields ~w(name)
  def changeset(stock, attrs) do
    stock
    |> cast(attrs, @required_fields)
    |> validate_required(@required_fields)
  end
end
