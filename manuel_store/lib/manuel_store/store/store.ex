defmodule ManuelStore.Store do
  @moduledoc """
  The Store context.
  """

  use ManuelStore, :context

  alias ManuelStore.Store.Product

  @doc """
  Returns the list of products.

  ## Examples

      iex> list_products()
      [%Product{}, ...]

  """
  def list_products, do: Repo.all(Product)

  @doc """
  Gets a single product.

  Raises `nil` if the Product does not exist.

  ## Examples

      iex> get_product(123)
      %Product{}

      iex> get_product(456)
      nil

  """
  def get_product(id), do: Repo.get(Product, id)

  @doc """
  Creates a product.

  ## Examples

      iex> create_product(%{field: value})
      {:ok, %Product{}}

      iex> create_product(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_product(attrs \\ %{}) do
    %Product{}
    |> Product.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a product.

  ## Examples

      iex> update_product(product, %{field: new_value})
      {:ok, %Product{}}

      iex> update_product(product, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_product(%Product{} = product, attrs) do
    product
    |> Product.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Product.

  ## Examples

      iex> delete_product(product)
      {:ok, %Product{}}

      iex> delete_product(product)
      {:error, %Ecto.Changeset{}}

  """
  def delete_product(%Product{} = product), do: Repo.delete(product)

  alias ManuelStore.Store.Stock

  @doc """
  Returns the list of stocks.

  ## Examples

      iex> list_stocks()
      [%Stock{}, ...]

  """
  def list_stocks, do: Repo.all(Stock)

  @doc """
  Gets a single stock.

  Raises `nil` if the Stock does not exist.

  ## Examples

      iex> get_stock(123)
      %Stock{}

      iex> get_stock(456)
      nil

  """
  def get_stock(id), do: Repo.get(Stock, id)

  @doc """
  Creates a stock.

  ## Examples

      iex> create_stock(%{field: value})
      {:ok, %Stock{}}

      iex> create_stock(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_stock(attrs \\ %{}) do
    %Stock{}
    |> Stock.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a stock.

  ## Examples

      iex> update_stock(stock, %{field: new_value})
      {:ok, %Stock{}}

      iex> update_stock(stock, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_stock(%Stock{} = stock, attrs) do
    stock
    |> Stock.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Stock.

  ## Examples

      iex> delete_stock(stock)
      {:ok, %Stock{}}

      iex> delete_stock(stock)
      {:error, %Ecto.Changeset{}}

  """
  def delete_stock(%Stock{} = stock), do: Repo.delete(stock)

  alias ManuelStore.Store.Price

  @doc """
  Returns the list of prices.

  ## Examples

      iex> list_prices()
      [%Price{}, ...]

  """
  def list_prices, do: Repo.all(Price)

  @doc """
  Gets a single price.

  Raises `nil` if the Price does not exist.

  ## Examples

      iex> get_price(123)
      %Price{}

      iex> get_price(456)
      nil

  """
  def get_price(id), do: Repo.get(Price, id)

  @doc """
  Creates a price.

  ## Examples

      iex> create_price(%{field: value})
      {:ok, %Price{}}

      iex> create_price(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_price(attrs \\ %{}) do
    %Price{}
    |> Price.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a price.

  ## Examples

      iex> update_price(price, %{field: new_value})
      {:ok, %Price{}}

      iex> update_price(price, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_price(%Price{} = price, attrs) do
    price
    |> Price.changeset(attrs)
    |> Repo.update()
  end

  alias ManuelStore.Store.Order

  @doc """
  Returns the list of orders.

  ## Examples

      iex> list_orders()
      [%Order{}, ...]

  """
  def list_orders, do: Repo.all(Order)

  @doc """
  Gets a new order of a buyer.

  Raises `nil` if the Order does not exist.

  ## Examples

      iex> get_order(%{user_id: 123})
      %Order{}

      iex> get_order(%{user_id: 456})
      nil

  """
  def get_order(%{user_id: user_id}) do
    Repo.get_by(Order, user_id: user_id, status: :new)
  end

  @doc """
  Gets a single order.

  Raises `nil` if the Order does not exist.

  ## Examples

      iex> get_order(123)
      %Order{}

      iex> get_order(456)
      nil

  """
  def get_order(id), do: Repo.get(Order, id)

  @doc """
  Creates a order.

  ## Examples

      iex> create_order(%{field: value})
      {:ok, %Order{}}

      iex> create_order(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_order(attrs \\ %{}) do
    %Order{}
    |> Order.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a order.

  ## Examples

      iex> update_order(order, %{field: new_value})
      {:ok, %Order{}}

      iex> update_order(order, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_order(%Order{} = order, attrs) do
    order
    |> Order.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Order.

  ## Examples

      iex> delete_order(order)
      {:ok, %Order{}}

      iex> delete_order(order)
      {:error, %Ecto.Changeset{}}

  """
  def delete_order(%Order{} = order), do: Repo.delete(order)

  alias ManuelStore.Store.StockProduct

  @doc """
  Returns the list of stocks_products.

  ## Examples

      iex> list_stocks_products()
      [%StockProduct{}, ...]

  """
  def list_stocks_products, do: Repo.all(StockProduct)

  @doc """
  Gets a single stock_product.

  Raises `nil` if the Stock product does not exist.

  ## Examples

      iex> get_stock_product(123)
      %StockProduct{}

      iex> get_stock_product(456)
      nil

  """
  def get_stock_product(%{id: id}), do: Repo.get(StockProduct, id)

  def get_stock_product(%{product_id: product_id, stock_id: stock_id}) do
    Repo.get_by(StockProduct, product_id: product_id, stock_id: stock_id)
  end

  def get_stock_product(%{quantity: quantity}) do
    Repo.get_by(StockProduct, quantity: quantity)
  end

  def get_stock_product(%{product_id: product_id, quantity: quantity}) do
    from(
      sp in StockProduct,
      where: sp.quantity >= ^quantity and sp.product_id == ^product_id,
      order_by: [desc: sp.quantity],
      limit: 1
    )
    |> Repo.one()
  end

  def get_stock_product(_, _), do: nil

  @doc """
  Creates a stock_product.

  ## Examples

      iex> create_stock_product(%{field: value})
      {:ok, %StockProduct{}}

      iex> create_stock_product(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_stock_product(attrs \\ %{}) do
    %StockProduct{}
    |> StockProduct.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a stock_product.

  ## Examples

      iex> update_stock_product(stock_product, %{field: new_value})
      {:ok, %StockProduct{}}

      iex> update_stock_product(stock_product, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_stock_product(%StockProduct{} = stock_product, attrs) do
    stock_product
    |> StockProduct.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a StockProduct.

  ## Examples

      iex> delete_stock_product(stock_product)
      {:ok, %StockProduct{}}

      iex> delete_stock_product(stock_product)
      {:error, %Ecto.Changeset{}}

  """
  def delete_stock_product(%StockProduct{} = stock_product), do: Repo.delete(stock_product)

  alias ManuelStore.Store.OrderProduct

  @doc """
  Returns the list of orders_products.

  ## Examples

      iex> list_orders_products()
      [%OrderProduct{}, ...]

  """
  def list_orders_products, do: Repo.all(OrderProduct)

  @doc """
  Gets a single order_product.

  Raises `nil` if the Order product does not exist.

  ## Examples

      iex> get_order_product(123)
      %OrderProduct{}

      iex> get_order_product(456)
      nil

  """
  def get_order_product(id), do: Repo.get(OrderProduct, id)

  @doc """
  Creates a order_product.

  ## Examples

      iex> create_order_product(%{field: value})
      {:ok, %OrderProduct{}}

      iex> create_order_product(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_order_product(attrs \\ %{}) do
    %OrderProduct{}
    |> OrderProduct.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a order_product.

  ## Examples

      iex> update_order_product(order_product, %{field: new_value})
      {:ok, %OrderProduct{}}

      iex> update_order_product(order_product, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_order_product(%OrderProduct{} = order_product, attrs) do
    order_product
    |> OrderProduct.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a OrderProduct.

  ## Examples

      iex> delete_order_product(order_product)
      {:ok, %OrderProduct{}}

      iex> delete_order_product(order_product)
      {:error, %Ecto.Changeset{}}

  """
  def delete_order_product(%OrderProduct{} = order_product), do: Repo.delete(order_product)
end
