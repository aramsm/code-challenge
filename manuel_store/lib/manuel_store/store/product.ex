defmodule ManuelStore.Store.Product do
  use ManuelStore, :schema

  schema "products" do
    field(:attributes, :map)
    field(:description, :string)
    field(:name, :string)

    has_many(:prices, Price)
    has_many(:orders_products, OrderProduct)
    has_many(:stocks_products, StockProduct)

    many_to_many(
      :stocks,
      Stock,
      join_through: "stocks_products",
      on_replace: :delete
    )

    timestamps()
  end

  @required_fields ~w(name description)
  @optional_fields ~w(attributes)
  @all_fields @required_fields ++ @optional_fields
  def changeset(product, attrs) do
    product
    |> cast(attrs, @all_fields)
    |> validate_required(@required_fields)
  end
end
