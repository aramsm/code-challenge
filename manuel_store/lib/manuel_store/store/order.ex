defmodule ManuelStore.Store.Order do
  use ManuelStore, :schema

  schema "orders" do
    field :buy_date, :naive_datetime
    field :delivery_fee, :integer
    field :status, OrderStatusEnum, default: :new

    belongs_to(:user, User)

    has_many(:orders_products, OrderProduct)

    timestamps()
  end

  @required_fields ~w(user_id status)
  @optional_fields ~w(buy_date)
  @all_fields @required_fields ++ @optional_fields
  def changeset(order, attrs) do
    order
    |> cast(attrs, @all_fields)
    |> validate_required(@required_fields)
  end
end
