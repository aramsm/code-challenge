defmodule ManuelStoreWeb do
  @moduledoc """
  The entrypoint for defining your web interface, such
  as controllers, views, channels and so on.

  This can be used in your application as:

      use ManuelStoreWeb, :controller
      use ManuelStoreWeb, :view

  The definitions below will be executed for every view,
  controller, etc, so keep them short and clean, focused
  on imports, uses and aliases.

  Do NOT define functions inside the quoted expressions
  below. Instead, define any helper function in modules
  and import those modules here.
  """

  def controller do
    quote do
      use Phoenix.Controller, namespace: ManuelStoreWeb

      import Plug.Conn
      import ManuelStoreWeb.Gettext
      alias ManuelStoreWeb.Router.Helpers, as: Routes
    end
  end

  def view do
    quote do
      use Phoenix.View,
        root: "lib/manuel_store_web/templates",
        namespace: ManuelStoreWeb

      # Import convenience functions from controllers
      import Phoenix.Controller, only: [get_flash: 1, get_flash: 2, view_module: 1]

      import ManuelStoreWeb.ErrorHelpers
      import ManuelStoreWeb.Gettext
      alias ManuelStoreWeb.Router.Helpers, as: Routes
    end
  end

  def router do
    quote do
      use Phoenix.Router
      import Plug.Conn
      import Phoenix.Controller
    end
  end

  def channel do
    quote do
      use Phoenix.Channel
      import ManuelStoreWeb.Gettext
    end
  end

  def graphql do
    quote do
      use Absinthe.Schema.Notation
      use Absinthe.Relay.Schema.Notation, :modern
      use Absinthe.Ecto, repo: ManuelStore.Repo

      import Absinthe.Ecto
      import Absinthe.Relay.Node

      alias ManuelStore.Accounts.User
      alias ManuelStoreWeb.Schema.Accounts

      alias ManuelStoreWeb.Schema.Accounts.UserResolver
      alias ManuelStoreWeb.Schema.Accounts.UserMutation
      alias ManuelStoreWeb.Schema.Accounts.UserQueries
      alias ManuelStoreWeb.Schema.Accounts.UserTypes

      alias ManuelStore.Store
      alias ManuelStoreWeb.Schema.Store

      alias ManuelStore.Store.{
        Order,
        OrderProduct,
        OrderStatusEnum,
        Price,
        Product,
        Stock,
        StockProduct
      }

      # Resolvers aliases
      alias ManuelStoreWeb.Schema.Store.{
        OrderResolver,
        OrderProductResolver,
        ProductResolver,
        PriceResolver,
        StockResolver,
        StockProductResolver
      }

      # Mutations aliases
      alias ManuelStoreWeb.Schema.Store.{
        OrderMutation,
        OrderProductMutation,
        ProductMutation,
        PriceMutation,
        StockMutation,
        StockProductMutation
      }

      # Queries aliases
      alias ManuelStoreWeb.Schema.Store.{
        OrderQueries,
        OrderProductQueries,
        ProductQueries,
        PriceQueries,
        StockQueries,
        StockProductQueries
      }

      # Types aliases
      alias ManuelStoreWeb.Schema.Store.{
        OrderTypes,
        OrderProductTypes,
        ProductTypes,
        PriceTypes,
        StockTypes,
        StockProductTypes
      }

      alias ManuelStoreWeb.Schema.Node.Queries.NodeQueries
      alias ManuelStoreWeb.Schema.Node.Types.NodeTypes

      alias ManuelStoreWeb.Schema.Scalars.JSONTypes
    end
  end

  def resolver do
    quote do
      alias Absinthe.Relay.Connection
      alias Absinthe.Resolution
      alias ManuelStore.Repo
      alias ManuelStore.Accounts
      alias ManuelStore.Accounts.User
      alias ManuelStore.Store

      alias ManuelStore.Store.{
        Order,
        OrderProduct,
        Price,
        Product,
        Stock,
        StockProduct
      }

      import Absinthe.Relay.Node
      import Absinthe.Resolution.Helpers
      import ManuelStoreWeb.Utils
      import Ecto
      import Ecto.Query
    end
  end

  @doc """
  When used, dispatch to the appropriate controller/view/etc.
  """
  defmacro __using__(which) when is_atom(which) do
    apply(__MODULE__, which, [])
  end
end
