defmodule ManuelStoreWeb.Schema.Types do
  use ManuelStoreWeb, :graphql

  # Accounts
  import_types(UserTypes)

  # Store
  import_types(OrderTypes)
  import_types(OrderProductTypes)
  import_types(ProductTypes)
  import_types(PriceTypes)
  import_types(StockTypes)
  import_types(StockProductTypes)

  # Node
  import_types(NodeTypes)

  # Scalars
  import_types(JSONTypes)
end
