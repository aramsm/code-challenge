defmodule ManuelStoreWeb.Schema.Accounts.UserMutation do
  use ManuelStoreWeb, :graphql

  object :user_mutations do
    payload field(:register) do
      input do
        field(:name, non_null(:string))
        field(:email, non_null(:string))
        field(:password, non_null(:string))
      end

      output do
        field(:user, :user)
      end

      resolve(&UserResolver.create/2)
    end

    payload field(:update) do
      input do
        field(:id, non_null(:id))
        field(:email, :string)
        field(:name, :string)
        field(:password, :string)
      end

      output do
        field(:user, :user)
      end

      middleware(Absinthe.Relay.Node.ParseIDs, id: :user)
      resolve(&UserResolver.update/2)
    end

    payload field(:delete) do
      input do
        field(:id, non_null(:id))
      end

      output do
        field(:user, :user)
      end

      middleware(Absinthe.Relay.Node.ParseIDs, id: :user)
      resolve(&UserResolver.delete/2)
    end

    payload field(:login) do
      input do
        field(:email, non_null(:string))
        field(:password, non_null(:string))
      end

      output do
        field(:session, :session)
      end

      resolve(&UserResolver.login/2)
    end
  end
end
