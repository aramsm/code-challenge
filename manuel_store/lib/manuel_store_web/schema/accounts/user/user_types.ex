defmodule ManuelStoreWeb.Schema.Accounts.UserTypes do
  use ManuelStoreWeb, :graphql

  @desc "An user of application"
  node object(:user) do
    field(:name, non_null(:string), description: "the name of the user")
    field(:email, non_null(:string), description: "the email of the user")
    field(:password, :string, description: "password used to authenticate the account")
    field(:inserted_at, non_null(:string), description: "The time the user was created.")
    field(:updated_at, :string, description: "The time the user was last updated.")

    # Associations

    @desc "The user's buy orders"
    field(:orders, list_of(:order), resolve: assoc(:orders))
  end

  object :session do
    field(:token, :string, description: "user session's token")
  end

  connection node_type: :user do
    field(:total_count, :integer)

    edge do
    end
  end
end
