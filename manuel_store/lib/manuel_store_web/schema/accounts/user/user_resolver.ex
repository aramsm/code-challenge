defmodule ManuelStoreWeb.Schema.Accounts.UserResolver do
  use ManuelStoreWeb, :resolver

  def all(args, _info) do
    User
    |> order_by([m], asc: m.name)
    |> paginate(args)
  end

  def find(param, _info) do
    param
    |> Accounts.get_user()
    |> case do
      %User{} = user -> {:ok, user}
      _ -> {:error, "not found"}
    end
  end

  def create(params, _info) do
    params
    |> Accounts.create_user()
    |> response(:user)
  end

  def update(%{id: id} = params, _info) do
    id
    |> Accounts.get_user()
    |> case do
      %User{} = user ->
        user
        |> Accounts.update_user(params)
        |> response(:user)

      _ ->
        {:error, "not found"}
    end
  end

  def update(_, _), do: {:error, "invalid params"}

  def delete(id, _info) do
    Accounts.get_user(id)
    |> case do
      %User{} = user ->
        user
        |> Accounts.delete_user()
        |> response(:user)

      _ ->
        {:error, "not found"}
    end
  end

  def login(%{email: email, password: password}, _info) do
    with {:ok, user} <- ManuelStore.Accounts.authenticate_user(email, password),
         {:ok, jwt, _} <- Guardian.encode_and_sign(:user, user, %{}, ttl: {1, :hour}),
         {:ok, _} <- ManuelStore.Accounts.update_user(user, %{token: jwt}) do
      {:ok, %{token: jwt}}
    end
  end
end
