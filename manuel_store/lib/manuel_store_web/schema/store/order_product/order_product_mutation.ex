defmodule ManuelStoreWeb.Schema.Store.OrderProductMutation do
  use ManuelStoreWeb, :graphql

  object :order_product_mutations do
    @desc "Adds items to the user's cart"
    # TODO: remove `user_id` when login system is on
    payload field(:create_order_product) do
      input do
        field(:quantity, non_null(:integer))
        field(:product_id, non_null(:id))
        field(:user_id, non_null(:id))
      end

      output do
        field(:order_product, :order_product)
      end

      middleware(Absinthe.Relay.Node.ParseIDs, product_id: :product)
      middleware(Absinthe.Relay.Node.ParseIDs, user_id: :user)
      resolve(&OrderProductResolver.create/2)
    end

    payload field(:update_order_product) do
      input do
        field(:id, non_null(:id))
        field(:quantity, non_null(:integer))
        field(:stock_product_id, :id)
      end

      output do
        field(:order_product, :order_product)
      end

      middleware(Absinthe.Relay.Node.ParseIDs, stock_product_id: :stock_product)
      middleware(Absinthe.Relay.Node.ParseIDs, id: :order_product)
      resolve(&OrderProductResolver.update/2)
    end

    payload field(:delete_order_product) do
      input do
        field(:id, non_null(:id))
      end

      output do
        field(:order_product, :order_product)
      end

      middleware(Absinthe.Relay.Node.ParseIDs, id: :order_product)
      resolve(&OrderProductResolver.delete/2)
    end
  end
end
