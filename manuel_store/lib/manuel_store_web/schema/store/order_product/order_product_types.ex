defmodule ManuelStoreWeb.Schema.Store.OrderProductTypes do
  use ManuelStoreWeb, :graphql

  @desc "The buy order's item"
  node object(:order_product) do
    @desc "Quantity of one type of product that the user wants to buy"
    field(:quantity, non_null(:integer))

    field(:inserted_at, non_null(:string), description: "The time the order_product was created.")
    field(:updated_at, :string, description: "The time the order_product was last updated.")

    # Associations

    @desc "The product's value"
    field(:unit_price, :price, resolve: assoc(:unit_price))

    @desc "The stock the item will come from"
    field(:stock_product, :stock_product, resolve: assoc(:stock_product))

    @desc "The product to be bought"
    field(:product, :product, resolve: assoc(:product))

    @desc "The buy order"
    field(:order, :order, resolve: assoc(:order))

    @desc "The buyer"
    field(:user, :user, resolve: assoc(:user))
  end

  connection node_type: :order_product do
    field(:total_count, :integer)

    edge do
    end
  end
end
