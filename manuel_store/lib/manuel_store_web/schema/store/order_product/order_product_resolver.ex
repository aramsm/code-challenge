defmodule ManuelStoreWeb.Schema.Store.OrderProductResolver do
  use ManuelStoreWeb, :resolver

  def all(args, _info) do
    OrderProduct
    |> order_by([m], desc: m.inserted_at)
    |> paginate(args)
  end

  def find(%{id: id}, _info) do
    id
    |> Store.get_order_product()
    |> case do
      %OrderProduct{} = order_product ->
        {
          :ok,
          order_product
          |> Repo.preload([:order, :stock_product, :product, :user, :unit_price])
        }

      _ ->
        {:error, "not found"}
    end
  end

  # TODO: use `Sage`
  def create(%{user_id: user_id, quantity: quantity} = params, _info) do
    is_product_available(params)
    |> case do
      %StockProduct{id: stock_product_id} = stock_product ->
        user_has_pending_order(user_id)
        |> case do
          %Order{id: order_id} ->
            response =
              Map.merge(params, %{order_id: order_id, stock_product_id: stock_product_id})
              |> Store.create_order_product()
              |> response(:order_product)

            update_stock_product_quantity(stock_product, quantity, "-")
            response

          _ ->
            %{user_id: user_id, status: :new}
            |> Store.create_order()
            |> case do
              {:ok, %Order{id: order_id}} ->
                response =
                  Map.merge(params, %{order_id: order_id, stock_product_id: stock_product_id})
                  |> Store.create_order_product()
                  |> response(:order_product)

                update_stock_product_quantity(stock_product, quantity, "-")
                response

              _ ->
                {:error, "unable to create order"}
            end
        end

      _ ->
        {:error, "product unavailable"}
    end
  end

  def create(_, _), do: {:error, "invalid params"}

  # TODO: use `Sage`
  def update(%{id: id, stock_product_id: stock_product_id} = params, _info) do
    new_stock_product =
      stock_product_id
      |> Store.get_stock_product()

    case new_stock_product[:quantity] >= params[:quantity] do
      true ->
        id
        |> Store.get_order_product()
        |> case do
          %OrderProduct{} = order_product ->
            old_quantity = order_product[:quantity]

            old_stock_product =
              order_product
              |> Repo.preload(:stock_product)
              |> Map.get(:stock_product)

            response =
              order_product
              |> Store.update_order_product(params)
              |> response(:order_product)

            update_stock_product_quantity(old_stock_product, old_quantity, "+")
            update_stock_product_quantity(new_stock_product, params[:quantity], "-")
            response

          _ ->
            {:error, "not found"}
        end

      false ->
        {:error, "product unavailable"}
    end
  end

  def update(%{id: id} = params, _info) do
    id
    |> Store.get_order_product()
    |> case do
      %OrderProduct{} = order_product ->
        old_stock_product =
          order_product
          |> Repo.preload(:stock_product)
          |> Map.get(:stock_product)

        case params[:quantity] > old_stock_product[:quantity] do
          true ->
            old_quantity = order_product[:quantity]

            is_product_available(%{
              product_id: order_product[:product_id],
              quantity: params[:quantity]
            })
            |> case do
              %StockProduct{} = stock_product ->
                response =
                  order_product
                  |> Store.update_order_product(params)
                  |> response(:order_product)

                update_stock_product_quantity(old_stock_product, old_quantity, "+")
                update_stock_product_quantity(stock_product, params[:quantity], "-")
                response

              _ ->
                {:error, "product unavailable"}
            end

          false ->
            response =
              order_product
              |> Store.update_order_product(params)
              |> response(:order_product)

            old_stock_product
            |> update_stock_product_quantity(params[:quantity], "-")

            response
        end

      _ ->
        {:error, "not found"}
    end
  end

  def update(_, _), do: {:error, "invalid params"}

  # TODO: use `Sage`
  def delete(%{id: id}, _info) do
    id
    |> Store.get_order_product()
    |> case do
      %OrderProduct{} = order_product ->
        response =
          order_product
          |> Store.delete_order_product()
          |> response(:order_product)

        stock_product =
          order_product
          |> Repo.preload(:stock_product)
          |> Map.get(:stock_product)

        update_stock_product_quantity(stock_product, order_product[:quantity], "+")
        response

      _ ->
        {:error, "not found"}
    end
  end

  def delete(_, _), do: {:error, "invalid params"}

  defp is_product_available(%{product_id: product_id, quantity: quantity}) do
    %{product_id: product_id, quantity: quantity}
    |> Store.get_stock_product()
  end

  defp user_has_pending_order(user_id), do: Store.get_order(%{user_id: user_id})

  defp update_stock_product_quantity(stock_product, quantity, sign) do
    new_quantity =
      sign
      |> case do
        "+" -> stock_product[:quantity] + quantity
        "-" -> stock_product[:quantity] - quantity
      end

    Store.update_stock_product(stock_product, %{quantity: new_quantity})
  end
end
