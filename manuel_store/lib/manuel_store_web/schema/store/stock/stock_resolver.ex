defmodule ManuelStoreWeb.Schema.Store.StockResolver do
  use ManuelStoreWeb, :resolver

  def all(args, _info) do
    Stock
    |> order_by([m], asc: m.name)
    |> paginate(args)
  end

  def find(%{id: id}, _info) do
    id
    |> Store.get_stock()
    |> case do
      %Stock{} = stock -> {:ok, stock}
      _ -> {:error, "not found"}
    end
  end

  def create(params, _info) do
    params
    |> Store.create_stock()
    |> response(:stock)
  end

  def update(%{id: id} = params, _info) do
    id
    |> Store.get_stock()
    |> case do
      %Stock{} = stock ->
        stock
        |> Store.update_stock(params)
        |> response(:stock)

      _ ->
        {:error, "not found"}
    end
  end

  def update(_, _), do: {:error, "invalid params"}

  def delete(%{id: id}, _info) do
    id
    |> Store.get_stock()
    |> case do
      %Stock{} = stock ->
        stock
        |> Store.delete_stock()
        |> response(:stock)

      _ ->
        {:error, "not found"}
    end
  end

  def delete(_, _), do: {:error, "invalid params"}
end
