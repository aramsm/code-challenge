defmodule ManuelStoreWeb.Schema.Store.StockTypes do
  use ManuelStoreWeb, :graphql

  @desc "The stock"
  node object(:stock) do
    field(:name, non_null(:string), description: "The stock's name")
    field(:inserted_at, non_null(:string), description: "The time the stock was created.")
    field(:updated_at, :string, description: "The time the stock was last updated.")

    # Associations

    @desc "The product"
    field(:products, list_of(:product), resolve: assoc(:products))
  end

  connection node_type: :stock do
    field(:total_count, :integer)

    edge do
    end
  end
end
