defmodule ManuelStoreWeb.Schema.Store.StockMutation do
  use ManuelStoreWeb, :graphql

  object :stock_mutations do
    payload field(:create_stock) do
      input do
        field(:name, non_null(:string))
      end

      output do
        field(:stock, :stock)
      end

      resolve(&StockResolver.create/2)
    end

    payload field(:update_stock) do
      input do
        field(:id, non_null(:id))
        field(:name, :string)
      end

      output do
        field(:stock, :stock)
      end

      middleware(Absinthe.Relay.Node.ParseIDs, id: :stock)
      resolve(&StockResolver.update/2)
    end

    payload field(:delete_stock) do
      input do
        field(:id, non_null(:id))
      end

      output do
        field(:stock, :stock)
      end

      middleware(Absinthe.Relay.Node.ParseIDs, id: :stock)
      resolve(&StockResolver.delete/2)
    end
  end
end
