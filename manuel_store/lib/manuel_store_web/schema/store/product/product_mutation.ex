defmodule ManuelStoreWeb.Schema.Store.ProductMutation do
  use ManuelStoreWeb, :graphql

  object :product_mutations do
    payload field(:create_product) do
      input do
        field(:attributes, non_null(:json))
        field(:description, non_null(:string))
        field(:name, non_null(:string))
      end

      output do
        field(:product, :product)
      end

      resolve(&ProductResolver.create/2)
    end

    payload field(:update_product) do
      input do
        field(:id, non_null(:id))
        field(:attributes, :json)
        field(:description, :string)
        field(:name, :string)
      end

      output do
        field(:product, :product)
      end

      middleware(Absinthe.Relay.Node.ParseIDs, id: :product)
      resolve(&ProductResolver.update/2)
    end

    payload field(:delete_product) do
      input do
        field(:id, non_null(:id))
      end

      output do
        field(:product, :product)
      end

      middleware(Absinthe.Relay.Node.ParseIDs, id: :product)
      resolve(&ProductResolver.delete/2)
    end
  end
end
