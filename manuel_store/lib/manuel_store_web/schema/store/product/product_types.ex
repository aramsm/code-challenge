defmodule ManuelStoreWeb.Schema.Store.ProductTypes do
  use ManuelStoreWeb, :graphql

  @desc "The store's products"
  node object(:product) do
    field(:attributes, :json, description: "Random attributes")
    field(:description, non_null(:string), description: "Product's description")
    field(:name, non_null(:string), description: "The name of the product")
    field(:inserted_at, non_null(:string), description: "The time the product was created.")
    field(:updated_at, :string, description: "The time the product was last updated.")

    # Associations

    @desc "The product's value"
    field(:prices, list_of(:price), resolve: assoc(:prices))

    @desc "Each item included in the user's buy order"
    field(:orders_products, list_of(:order_product), resolve: assoc(:orders_products))

    @desc "Quantity available in each stock"
    field(:stocks_products, list_of(:stock_product), resolve: assoc(:stocks_products))
  end

  connection node_type: :product do
    field(:total_count, :integer)

    edge do
    end
  end
end
