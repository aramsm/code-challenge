defmodule ManuelStoreWeb.Schema.Store.ProductResolver do
  use ManuelStoreWeb, :resolver

  def all(args, _info) do
    Product
    |> order_by([m], asc: m.name)
    |> paginate(args)
  end

  def find(%{id: id}, _info) do
    id
    |> Store.get_product()
    |> case do
      %Product{} = product ->
        {:ok, product |> Repo.preload([:price])}

      _ ->
        {:error, "not found"}
    end
  end

  def create(params, _info) do
    params
    |> Store.create_product()
    |> response(:product)
  end

  def update(%{id: id} = params, _info) do
    id
    |> Store.get_product()
    |> case do
      %Product{} = product ->
        product
        |> Store.update_product(params)
        |> response(:product)

      _ ->
        {:error, "not found"}
    end
  end

  def update(_, _), do: {:error, "invalid params"}

  def delete(%{id: id}, _info) do
    id
    |> Store.get_product()
    |> case do
      %Product{} = product ->
        product
        |> Store.delete_product()
        |> response(:product)

      _ ->
        {:error, "not found"}
    end
  end

  def delete(_, _), do: {:error, "invalid params"}
end
