defmodule ManuelStoreWeb.Schema.Store.PriceTypes do
  use ManuelStoreWeb, :graphql

  @desc "The product's price"
  node object(:price) do
    field(:value, non_null(:integer), description: "The price in cents")
    field(:currency, non_null(:string), description: "Which currency is the price")
    field(:inserted_at, non_null(:string), description: "The time the price was created.")
    field(:updated_at, :string, description: "The time the price was last updated.")

    # Associations

    @desc "The product"
    field(:product, :product, resolve: assoc(:product))
  end

  connection node_type: :price do
    field(:total_count, :integer)

    edge do
    end
  end
end
