defmodule ManuelStoreWeb.Schema.Store.PriceMutation do
  use ManuelStoreWeb, :graphql

  object :price_mutations do
    payload field(:create) do
      input do
        field(:value, non_null(:integer))
        field(:currency, non_null(:string))
        field(:product_id, non_null(:id))
      end

      output do
        field(:price, :price)
      end

      middleware(Absinthe.Relay.Node.ParseIDs, product_id: :product)
      resolve(&PriceResolver.create/2)
    end

    payload field(:update) do
      input do
        field(:id, non_null(:id))
        field(:value, non_null(:integer))
        field(:currency, non_null(:string))
        field(:product_id, non_null(:id))
      end

      output do
        field(:price, :price)
      end

      middleware(Absinthe.Relay.Node.ParseIDs, id: :price)
      middleware(Absinthe.Relay.Node.ParseIDs, product_id: :product)
      resolve(&PriceResolver.update/2)
    end
  end
end
