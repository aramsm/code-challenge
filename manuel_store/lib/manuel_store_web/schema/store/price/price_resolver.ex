defmodule ManuelStoreWeb.Schema.Store.PriceResolver do
  use ManuelStoreWeb, :resolver

  def create(params, _info) do
    params
    |> Store.create_price()
    |> response(:price)
  end

  def update(%{id: id} = params, _info) do
    id
    |> Store.get_price()
    |> case do
      %Price{} = price ->
        price
        |> Store.update_price(params)
        |> response(:price)

      _ ->
        {:error, "not found"}
    end
  end

  def update(_, _), do: {:error, "invalid params"}
end
