defmodule ManuelStoreWeb.Schema.Store.StockProductTypes do
  use ManuelStoreWeb, :graphql

  @desc "The product's items available in a stock"
  node object(:stock_product) do
    field(:quantity, non_null(:integer), description: "The items quantity available")
    field(:inserted_at, non_null(:string), description: "The time the stock_product was created.")
    field(:updated_at, :string, description: "The time the stock_product was last updated.")

    # Associations

    @desc "The product"
    field(:product, :product, resolve: assoc(:product))

    @desc "The stock"
    field(:stock, :stock, resolve: assoc(:stock))

    @desc "All items buy orders from this stock"
    field(:orders_products, list_of(:order_product), resolve: assoc(:orders_products))
  end

  connection node_type: :stock_product do
    field(:total_count, :integer)

    edge do
    end
  end
end
