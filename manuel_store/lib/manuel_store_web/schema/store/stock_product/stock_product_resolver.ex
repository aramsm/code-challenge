defmodule ManuelStoreWeb.Schema.Store.StockProductResolver do
  use ManuelStoreWeb, :resolver

  def all(args, _info) do
    StockProduct
    |> order_by([m], asc: m.quantity)
    |> paginate(args)
  end

  def find(params, _info) do
    params
    |> Store.get_stock_product()
    |> case do
      %StockProduct{} = stock_product ->
        {:ok, stock_product |> Repo.preload([:stock, :product])}

      _ ->
        {:error, "not found"}
    end
  end

  def create(params, _info) do
    params
    |> Store.create_stock_product()
    |> response(:stock_product)
  end

  def update(%{id: id} = params, _info) do
    id
    |> Store.get_stock_product()
    |> case do
      %StockProduct{} = stock_product ->
        stock_product
        |> Store.update_stock_product(params)
        |> response(:stock_product)

      _ ->
        {:error, "not found"}
    end
  end

  def update(_, _), do: {:error, "invalid params"}

  def delete(%{id: id}, _info) do
    id
    |> Store.get_stock_product()
    |> case do
      %StockProduct{} = stock_product ->
        stock_product
        |> Store.delete_stock_product()
        |> response(:stock_product)

      _ ->
        {:error, "not found"}
    end
  end

  def delete(_, _), do: {:error, "invalid params"}
end
