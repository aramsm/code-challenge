defmodule ManuelStoreWeb.Schema.Store.StockProductMutation do
  use ManuelStoreWeb, :graphql

  object :stock_product_mutations do
    @desc "Adds itens to a specific stock"
    payload field(:create_stock_product) do
      input do
        field(:quantity, non_null(:integer))
        field(:product_id, non_null(:id))
        field(:stock_product_id, non_null(:id))
      end

      output do
        field(:stock_product, :stock_product)
      end

      middleware(Absinthe.Relay.Node.ParseIDs, product_id: :product)
      middleware(Absinthe.Relay.Node.ParseIDs, stock_id: :stock)
      resolve(&StockProductResolver.create/2)
    end

    payload field(:update_stock_product) do
      input do
        field(:id, non_null(:id))
        field(:quantity, non_null(:integer))
      end

      output do
        field(:stock_product, :stock_product)
      end

      middleware(Absinthe.Relay.Node.ParseIDs, id: :stock_product)
      resolve(&StockProductResolver.update/2)
    end

    payload field(:delete_stock_product) do
      input do
        field(:id, non_null(:id))
      end

      output do
        field(:stock_product, :stock_product)
      end

      middleware(Absinthe.Relay.Node.ParseIDs, id: :stock_product)
      resolve(&StockProductResolver.delete/2)
    end
  end
end
