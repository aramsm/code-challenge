defmodule ManuelStoreWeb.Schema.Store.OrderTypes do
  use ManuelStoreWeb, :graphql

  @desc "Order status types"
  enum :order_status_enum do
    value(:new)
    value(:approved)
    value(:cancelled)
    value(:delivered)
  end

  @desc "The user's buy orders"
  node object(:order) do
    field(:buy_date, non_null(:string), description: "When the order was created")
    field(:delivery_fee, non_null(:integer), description: "How much user will pay for delivery")
    field(:status, non_null(:order_status_enum), description: "The current order' status")
    field(:inserted_at, non_null(:string), description: "The time the order was created.")
    field(:updated_at, :string, description: "The time the order was last updated.")

    # Associations

    @desc "The order's owner"
    field(:user, :user, resolve: assoc(:user))

    @desc "Each item included in the user's buy order"
    field(:orders_products, list_of(:order_product), resolve: assoc(:orders_products))
  end

  connection node_type: :order do
    field(:total_count, :integer)

    edge do
    end
  end
end
