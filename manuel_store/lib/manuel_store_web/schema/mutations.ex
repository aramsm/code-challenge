defmodule ManuelStoreWeb.Schema.Mutations do
  use ManuelStoreWeb, :graphql

  # Accounts
  import_types(UserMutation)

  # Store
  import_types(OrderProductMutation)
  import_types(ProductMutation)
  import_types(PriceMutation)
  import_types(StockMutation)
  import_types(StockProductMutation)
end
