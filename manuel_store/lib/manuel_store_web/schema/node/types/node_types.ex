defmodule ManuelStoreWeb.Schema.Node.Types.NodeTypes do
  @moduledoc """
  Provides a Node Type for use in a GraphQL Schema. This is required by any
  Relay-compliant server as Relay needs to refetch items by a base64-encoded
  id.
  """

  use ManuelStoreWeb, :graphql

  node interface do
    resolve_type(fn
      # Accounts
      %User{}, _ ->
        :user

      # Store
      %Order{}, _ ->
        :order

      %OrderProduct{}, _ ->
        :order_product

      %Product{}, _ ->
        :product

      %Price{}, _ ->
        :price

      %Stock{}, _ ->
        :stock

      %StockProduct{}, _ ->
        :stock_product

      # anything else
      _, _ ->
        nil
    end)
  end
end
