defmodule ManuelStoreWeb.Schema do
  use Absinthe.Schema
  use Absinthe.Relay.Schema, :modern

  import_types(ManuelStoreWeb.Schema.Types)
  import_types(ManuelStoreWeb.Schema.Queries)
  import_types(ManuelStoreWeb.Schema.Mutations)

  query do
  end
end
