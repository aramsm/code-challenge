defmodule ManuelStoreWeb.Router do
  use ManuelStoreWeb, :router

  pipeline :context do
    plug ManuelStoreWeb.Authentication.Context
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api" do
    pipe_through([:context, :api])

    forward("/graphql", Absinthe.Plug, schema: ManuelStoreWeb.Schema)

    if Mix.env() == :dev do
      forward("/graphiql", Absinthe.Plug.GraphiQL, schema: ManuelStoreWeb.Schema)
    end
  end
end
