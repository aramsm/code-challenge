defmodule ManuelStoreWeb.Authentication.Context do
  @behaviour Plug

  use ManuelStoreWeb, :controller

  alias ManuelStore.Repo
  alias ManuelStore.Accounts.User

  def init(opts), do: opts

  def call(conn, _) do
    context = build_context(conn)
    put_private(conn, :absinthe, %{context: context})
  end

  defp build_context(conn) do
    with ["Bearer" <> token] <- get_req_header(conn, "authorization"),
    {:ok, current_user} <- authorize(token) do
      %{current_user: current_user}
    else
      _ -> %{}
    end
  end

  defp authorize(token) do
    Repo.get_by(User, token: token)
    |> case do
      nil  -> {:error, :unauthorized}
      user -> {:ok, user}
    end
  end
end
