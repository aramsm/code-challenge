defmodule ManuelStoreWeb.Utils do
  alias ManuelStore.Repo
  alias Absinthe.Relay.Connection

  def paginate(query, pagination_args) do
    with {:ok, data} <- Connection.from_query(query, &Repo.all/1, pagination_args) do
      total_count = Repo.aggregate(query, :count, :id)
      {:ok, data |> Map.put(:total_count, total_count)}
    else
      err -> err
    end
  end

  def response(data, node) when is_atom(node) do
    data
    |> case do
      {:ok, data} -> {:ok, %{"#{node}": data}}
      {:error, changeset} -> {:error, %{message: changeset.errors}}
    end
  end
end
