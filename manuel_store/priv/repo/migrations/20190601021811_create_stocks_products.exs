defmodule ManuelStore.Repo.Migrations.CreateStocksProducts do
  use Ecto.Migration

  def change do
    create table(:stocks_products, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :quantity, :integer, null: false

      add :product_id, references(:products, on_delete: :delete_all, type: :binary_id),
        null: false

      add :stock_id, references(:stocks, on_delete: :delete_all, type: :binary_id), null: false

      timestamps()
    end

    create constraint(:stocks_products, :quantity_must_be_positive, check: "quantity >= 0")
    create index(:stocks_products, [:product_id])
    create index(:stocks_products, [:stock_id])
  end
end
