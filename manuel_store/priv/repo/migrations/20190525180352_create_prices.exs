defmodule ManuelStore.Repo.Migrations.CreatePrices do
  use Ecto.Migration

  def change do
    create table(:prices, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :currency, :string
      add :value, :integer

      add :product_id, references(:products, on_delete: :delete_all, type: :binary_id),
        null: false

      timestamps()
    end

    create constraint(:prices, :value_must_be_positive, check: "value > 0")
    create index(:prices, [:product_id])
  end
end
