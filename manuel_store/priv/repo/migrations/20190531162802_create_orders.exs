defmodule ManuelStore.Repo.Migrations.CreateOrders do
  use Ecto.Migration

  alias ManuelStore.Store.OrderStatusEnum

  def change do
    OrderStatusEnum.create_type()

    create table(:orders, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :buy_date, :naive_datetime
      add :status, OrderStatusEnum.type(), null: false, default: "new"
      add :delivery_fee, :integer
      add :user_id, references(:users, on_delete: :nothing, type: :binary_id), null: false

      timestamps()
    end

    create constraint(:orders, :delivery_fee_must_be_positive, check: "delivery_fee > 0")
    create index(:orders, [:user_id])
  end
end
