defmodule ManuelStore.Repo.Migrations.CreateOrdersProducts do
  use Ecto.Migration

  def change do
    create table(:orders_products, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :quantity, :integer, null: false

      add :order_id,
          references(:orders, on_delete: :delete_all, type: :binary_id),
          null: false

      add :product_id,
          references(:products, on_delete: :delete_all, type: :binary_id),
          null: false

      add :stock_product_id,
          references(:stocks_products, on_delete: :delete_all, type: :binary_id),
          null: false

      timestamps()
    end

    create constraint(:orders_products, :quantity_must_be_positive, check: "quantity > 0")
    create index(:orders_products, [:order_id])
    create index(:orders_products, [:product_id])
    create index(:orders_products, [:stock_product_id])
  end
end
